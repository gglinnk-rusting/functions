fn main() {
    println!("Hello, world!");
    let val_one = {
        let x = 49;
        x + 1
    };
    add(val_one, value_of(-20));
}

fn value_of(x: i32) -> i32 {
    x
}

fn add(x: i32, y: i32) -> i32 {
    println!("Another function");
    println!("X and Y values are {}, {}.", x, y);
    x + y
}
